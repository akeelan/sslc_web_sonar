import { Component, OnInit } from '@angular/core';

import { IUser } from './user';
import { UserService } from './user.service';

@Component({
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  
  errorMessage = '';
  users: IUser[] = [
    {
      "id" : 1,
      "name" : "Jeya",
      "email" : "jeya@email.com",
      "roles" : ['Producer']
    },
    {
      "id" : 2,
      "name" : "ZQ",
      "email" : "zq@email.com",
      "roles" : ['Admin', 'Manager']
    }
  ];

  constructor(private userService: UserService) {

  }

  ngOnInit(): void {
    // this.userService.getUsers().subscribe(
    //   products => {
    //     this.users = this.users;
    //   },
    //   error => this.errorMessage = <any>error
    // );
  }
}
