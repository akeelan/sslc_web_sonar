import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { IUser } from './user';
import { UserService } from './user.service';

@Component({
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  errorMessage = '';
  user: IUser = {
    "id" : null,
    "name" : "",
    "email" : "",
    "roles" : []
  };

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) {
  }

  ngOnInit() {
    const param = this.route.snapshot.paramMap.get('id');
    if (param) {
      const id = +param;
      this.getUser(id);
    }
  }

  getUser(id: number) {
    // this.userService.getProduct(id).subscribe(
    //   product => this.product = product,
    //   error => this.errorMessage = <any>error);
    this.user = {
      "id" : 2,
      "name" : "ZQ",
      "email" : "zq@email.com",
      "roles" : ['Admin', 'Manager']
    };
  }

  onBack(): void {
    this.router.navigate(['/users']);
  }

}
