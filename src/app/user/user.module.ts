import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { UserListComponent } from './user-list.component';
import { UserDetailComponent } from './user-detail.component';
import { ProductDetailGuard } from './user-detail.guard';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forChild([
      { path: 'users', component: UserListComponent },
      {
        path: 'users/:id',
        canActivate: [ProductDetailGuard],
        component: UserDetailComponent
      },
    ])
  ],
  declarations: [
    UserListComponent,
    UserDetailComponent
  ]
})
export class UserModule { }
